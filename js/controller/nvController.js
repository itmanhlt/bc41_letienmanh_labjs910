function getEl(id) {
  return document.getElementById(id);
}
function layThongTinTuForm() {
  let taiKhoan = getEl("tknv").value;
  let tenNV = getEl("name").value;
  let emailNV = getEl("email").value;
  let passNV = getEl("password").value;
  let ngayLam = getEl("datepicker").value;
  let luong = getEl("luongCB").value * 1;
  let chucVu = getEl("chucvu").value;
  let gioLam = getEl("gioLam").value * 1;

  return new NhanVien(
    taiKhoan,
    tenNV,
    emailNV,
    passNV,
    ngayLam,
    luong,
    chucVu,
    gioLam
  );
}

//render dsnv
function render(arr) {
  let contentHTML = "";
  for (let i = 0; i < arr.length; i++) {
    let nv = arr[i];
    let contentTr = `<tr>
    <td>${nv.taiKhoan}</td>
    <td>${nv.tenNV}</td>
    <td>${nv.emailNV}</td>
    <td>${nv.ngayLam}</td>
    <td>${nv.chucVu}</td>
    <td>${nv.tongLuong().toLocaleString()}</td>
    <td id = "xepLoai">${nv.xepLoai()}</td>
    <td class = "d-flex">
    <button onclick = "deleteNV('${
      nv.taiKhoan
    }')" id = "btnXoa" class = "btn btn-danger mr-2">Xóa</button>
    <button onclick = "editNV('${
      nv.taiKhoan
    }')" id = "btnSua" class = "btn btn-warning" data-toggle="modal"
    data-target="#myModal">Sửa</button>
    </td>
    </tr>`;
    contentHTML += contentTr;
  }
  return (getEl("tableDanhSach").innerHTML = contentHTML);
}

function searchLocationNV(taiKhoan, arr) {
  var viTri = -1;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].taiKhoan == taiKhoan) {
      viTri = i;
    }
  }
  return viTri;
}
