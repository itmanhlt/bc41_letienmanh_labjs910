var error = [
  "Vui lòng nhập tài khoản",
  "Vui lòng nhập tên",
  "Vui lòng nhập email",
  "Vui lòng nhập mật khẩu",
  "Vui lòng nhập ngày làm",
  "Vui lòng lương cơ bản",
  "Vui lòng chọn chức vụ",
  "Vui lòng nhập giờ làm",
];

// get element
function getEl(id) {
  return document.getElementById(id);
}

// search location
function searchLocationNV(taiKhoan, arr) {
  var viTri = -1;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].taiKhoan == taiKhoan) {
      viTri = i;
    }
  }
  return viTri;
}

//check duplicate
function checkDuplicate(el, idNotify, arr) {
  var viTri = searchLocationNV(el, arr);
  if (viTri != -1) {
    return (
      (getEl(idNotify).innerHTML = `Tài khoản đã tồn tại`),
      (getEl(idNotify).style.display = "block"),
      false
    );
  } else {
    return (
      (getEl(idNotify).innerHTML = ""),
      (getEl(idNotify).style.display = "none"),
      true
    );
  }
}

//check empty
function checkEmpty(el, message, idNotify) {
  if (el == "") {
    return (
      (getEl(idNotify).innerHTML = error[message]),
      (getEl(idNotify).style.display = "block"),
      false
    );
  } else {
    return (
      (getEl(idNotify).innerHTML = ""),
      (getEl(idNotify).style.display = "none"),
      true
    );
  }
}

//check length
function checkLength(el, min, max, idNotify) {
  if (el.length < min || el.length > max) {
    return (
      (getEl(idNotify).innerHTML = `Tài khoản tối đa ${min} - ${max} ký số`),
      (getEl(idNotify).style.display = "block"),
      false
    );
  } else {
    return (
      (getEl(idNotify).innerHTML = ""),
      (getEl(idNotify).style.display = "none"),
      true
    );
  }
}

// check all letters
function checkLetter(el, idNotify) {
  const re =
    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
  var isValid = re.test(el);
  if (isValid) {
    return (
      (getEl(idNotify).style.display = "none"),
      (getEl(idNotify).innerHTML = ""),
      true
    );
  } else {
    return (
      (getEl(idNotify).innerHTML = `Tên nhân viên phải là chữ`),
      (getEl(idNotify).style.display = "block"),
      false
    );
  }
}

// check email
function checkEmail(el, idNotify) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isValid = re.test(el);
  if (isValid) {
    return (
      (getEl(idNotify).style.display = "none"),
      (getEl(idNotify).innerHTML = ""),
      true
    );
  } else {
    return (
      (getEl(idNotify).innerHTML = `Email phải đúng định đạng`),
      (getEl(idNotify).style.display = "block"),
      false
    );
  }
}

// check password
function checkPassword(el, idNotify) {
  const re = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,10}$/;
  var isValid = re.test(el);
  if (isValid) {
    return (
      (getEl(idNotify).style.display = "none"),
      (getEl(idNotify).innerHTML = ""),
      true
    );
  } else {
    return (
      (getEl(
        idNotify
      ).innerHTML = `Mật khẩu chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt`),
      (getEl(idNotify).style.display = "block"),
      false
    );
  }
}

//check date format
function checkDateFormat(el, idNotify) {
  const re =
    /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
  var isValid = re.test(el);
  if (isValid) {
    return (
      (getEl(idNotify).style.display = "none"),
      (getEl(idNotify).innerHTML = ""),
      true
    );
  } else {
    return (
      (getEl(idNotify).innerHTML = `Ngày làm không đúng định dạng mm/dd/yyyy`),
      (getEl(idNotify).style.display = "block"),
      false
    );
  }
}

//check salary
function checkSalary(el, min, max, idNotify) {
  if (el < min || el > max) {
    return (
      (getEl(
        idNotify
      ).innerHTML = `Lương cơ bản ${min.toLocaleString()} - ${max.toLocaleString()}`),
      (getEl(idNotify).style.display = "block"),
      false
    );
  } else {
    return (
      (getEl(idNotify).innerHTML = ""),
      (getEl(idNotify).style.display = "none"),
      true
    );
  }
}

//check hour work
function checkHourWork(el, min, max, idNotify) {
  if (el < min || el > max) {
    return (
      (getEl(idNotify).innerHTML = `Số giờ làm tron tháng ${min} - ${max} giờ`),
      (getEl(idNotify).style.display = "block"),
      false
    );
  } else {
    return (
      (getEl(idNotify).innerHTML = ""),
      (getEl(idNotify).style.display = "none"),
      true
    );
  }
}
