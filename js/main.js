var DSNV;
var storageKey = "dsnv";
var dataStorage = localStorage.getItem(storageKey);
if (dataStorage) {
  var dataArr = JSON.parse(dataStorage);
  DSNV = dataArr.map(function (item) {
    var nv = new NhanVien(
      item.taiKhoan,
      item.tenNV,
      item.emailNV,
      item.passNV,
      item.ngayLam,
      item.luong,
      item.chucVu,
      item.gioLam
    );
    return nv;
  });
} else {
  DSNV = [];
}
//render dsnv
render(DSNV);
// get el
function getEl(id) {
  return document.getElementById(id);
}
// set local storage
function setLocalStorage() {
  return localStorage.setItem(storageKey, JSON.stringify(DSNV));
}

// clear text
function clearText() {
  getEl("tknv").value = "";
  getEl("name").value = "";
  getEl("email").value = "";
  getEl("password").value = "";
  getEl("datepicker").value = "";
  getEl("luongCB").value = "";
  getEl("chucvu").value = "";
  getEl("gioLam").value = "";
}
// validation
function validation() {
  var nv = layThongTinTuForm();
  var isValid = true;
  isValid =
    checkEmpty(nv.taiKhoan, 0, "tbTKNV") &&
    checkLength(nv.taiKhoan, 4, 6, "tbTKNV") &&
    checkDuplicate(nv.taiKhoan, "tbTKNV", DSNV);
  isValid &= checkEmpty(nv.tenNV, 1, "tbTen") && checkLetter(nv.tenNV, "tbTen");
  isValid &=
    checkEmpty(nv.emailNV, 2, "tbEmail") && checkEmail(nv.emailNV, "tbEmail");
  isValid &=
    checkEmpty(nv.passNV, 3, "tbMatKhau") &&
    checkLength(nv.passNV, 6, 10, "tbMatKhau") &&
    checkPassword(nv.passNV, "tbMatKhau");
  isValid &=
    checkEmpty(nv.ngayLam, 4, "tbNgay") &&
    checkDateFormat(nv.ngayLam, "tbNgay");
  isValid &=
    checkEmpty(nv.luong, 5, "tbLuongCB") &&
    checkSalary(nv.luong, 1000000, 20000000, "tbLuongCB");
  isValid &= checkEmpty(nv.chucVu, 6, "tbChucVu");
  isValid &=
    checkEmpty(nv.gioLam, 7, "tbGiolam") &&
    checkHourWork(nv.gioLam, 80, 200, "tbGiolam");
  return isValid;
}

// add nv
function addNV() {
  var nv = layThongTinTuForm();
  //validation
  var isValid = validation();
  //check validation
  if (isValid) {
    // push nv to dsnv
    DSNV.push(nv);
    // set local storage
    setLocalStorage();
    // render dsnv
    render(DSNV);
    //clear text
    clearText();
  }
}

//delete nv
function deleteNV(taiKhoan) {
  var viTri = searchLocationNV(taiKhoan, DSNV);
  if (viTri != -1) {
    DSNV.splice(viTri, 1);
    // set local storage
    setLocalStorage();
    //render dsnv
    render(DSNV);
  }
}

//show nv to form
function editNV(taiKhoan) {
  var viTri = searchLocationNV(taiKhoan, DSNV);
  if (viTri != -1) {
    getEl("tknv").value = DSNV[viTri].taiKhoan;
    getEl("tknv").disabled = true;
    getEl("name").value = DSNV[viTri].tenNV;
    getEl("email").value = DSNV[viTri].emailNV;
    getEl("password").value = DSNV[viTri].passNV;
    getEl("datepicker").value = DSNV[viTri].ngayLam;
    getEl("luongCB").value = DSNV[viTri].luong;
    getEl("chucvu").value = DSNV[viTri].chucVu;
    getEl("gioLam").value = DSNV[viTri].gioLam;
    getEl("btnThemNV").disabled = true;
  }
}

// update nv
function updateNV() {
  var nv = layThongTinTuForm();
  var taiKhoan = getEl("tknv").value;
  var viTri = searchLocationNV(taiKhoan, DSNV);
  // validation
  var isValid = true;
  isValid =
    checkEmpty(nv.taiKhoan, 0, "tbTKNV") &&
    checkLength(nv.taiKhoan, 4, 6, "tbTKNV");
  isValid &= checkEmpty(nv.tenNV, 1, "tbTen") && checkLetter(nv.tenNV, "tbTen");
  isValid &=
    checkEmpty(nv.emailNV, 2, "tbEmail") && checkEmail(nv.emailNV, "tbEmail");
  isValid &=
    checkEmpty(nv.passNV, 3, "tbMatKhau") &&
    checkLength(nv.passNV, 6, 10, "tbMatKhau") &&
    checkPassword(nv.passNV, "tbMatKhau");
  isValid &=
    checkEmpty(nv.ngayLam, 4, "tbNgay") &&
    checkDateFormat(nv.ngayLam, "tbNgay");
  isValid &=
    checkEmpty(nv.luong, 5, "tbLuongCB") &&
    checkSalary(nv.luong, 1000000, 20000000, "tbLuongCB");
  isValid &= checkEmpty(nv.chucVu, 6, "tbChucVu");
  isValid &=
    checkEmpty(nv.gioLam, 7, "tbGiolam") &&
    checkHourWork(nv.gioLam, 80, 200, "tbGiolam");
  for (var i = 0; i < DSNV.length; i++) {
    //validation
    if (DSNV[i].taiKhoan == taiKhoan) {
      if (isValid) {
        DSNV.splice(viTri, 1, nv);
        // set local storage
        setLocalStorage();
        //render dsnv
        render(DSNV);
        // reload page
        location.reload();
      }
    }
  }
}

//search follow type nv
function search() {
  var searchValue = document.getElementById("searchName").value;
  var userSearch = DSNV.filter((value) => {
    return value.xepLoai().toUpperCase().includes(searchValue.toUpperCase());
  });
  if (userSearch.length != 0) {
    render(userSearch);
  }
}

document.getElementById("btnDong").addEventListener("click", function () {
  location.reload();
});

document.getElementById("btnThem").addEventListener("click", function () {
  getEl("btnCapNhat").disabled = true;
});
