function NhanVien(
  taiKhoan,
  tenNV,
  emailNV,
  passNV,
  ngayLam,
  luong,
  chucVu,
  gioLam
) {
  this.taiKhoan = taiKhoan;
  this.tenNV = tenNV;
  this.emailNV = emailNV;
  this.passNV = passNV;
  this.emailNV = emailNV;
  this.passNV = passNV;
  this.ngayLam = ngayLam;
  this.luong = luong;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tongLuong = function () {
    if (this.chucVu == "Sếp") {
      return this.luong * 3;
    }
    if (this.chucVu == "Trưởng phòng") {
      return this.luong * 2;
    }
    if (this.chucVu == "Nhân viên") {
      return this.luong;
    }
  };
  this.xepLoai = function () {
    if (this.gioLam >= 192) {
      return "Xuất sắc";
    }
    if (this.gioLam >= 176) {
      return "Giỏi";
    }
    if (this.gioLam >= 160) {
      return "Khá";
    }
    if (this.gioLam < 160) {
      return "Trung bình";
    }
  };
}
